#!/bin/bash -e

##############################################################
# This script is used to prepare the device for testing      #
#                                                            #
#                                                            #
# For information, how to use this script, run               #
#   asterixFlasher.sh -h                                     #
#                                                            #
#                                                            #
# Author: Hasan Ahmed Tanveer                                #
#         <hasanahmed.tanveer@elektrobit.com                 #
#                                                            #
##############################################################


function my_func() {
          echo "called((("
	  A=${SIMULATIX_VERSION}
          B=${NO}
          C=${Test}
          
          echo ${A}
          echo ${B}
          echo ${C} 
	
}


# --Usage of the script-- #
function usage {
    echo "Usage: $0 [Parameters]"
    echo ""
    echo "     -h, --help     Print this help."
    echo ""
    echo "     --call         Calling standalone functions in the script."
    echo "                    Callable functions: runTest, postBuildCleanUp"
    echo ""
}

if [ 0 -eq $# ] ; then
    usage
    exit 1
fi

# Get and parse options using /usr/bin/getop
OPTIONS=$(getopt -o h --long help,call: -n "$0" -- "$@")

eval set -- "$OPTIONS"

while true ; do
    case "$1" in
        -h|--help) usage; exit 0 ;;
        --call) eval ${2} ; exit 0 ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done


