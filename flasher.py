#!/usr/bin/env python3
from abc import ABC, abstractmethod


class Flasher(ABC):

    def __init__(self):
        print("init ", Flasher.__class__)

    @abstractmethod
    def get_environment(self):
        pass

    @abstractmethod
    def flash_device(self):
        pass

    @abstractmethod
    def reboot_device(self):
        pass

    @abstractmethod
    def wait_for_device(self):
        pass

