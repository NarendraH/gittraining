#!/usr/bin/python -u

###############################################################################################
# The interface is implemented to use as a initiator of flashing from Jenkins Flasher Jobs.   #
# This interface is called from Jenkins Flasher Jobs and reading the configuration parameters #
# from the Config YML file.                                                                   #
# Config parameters is passed to respective flasher shell script to start the flashing.       #
# Author: Narendra Harny                                                                      #
# Email: narendra.harny@elektrobit.com                                                        #
###############################################################################################

import sys
import yaml
import os


def config_file_issue():
    """
    Handling config file issue here
    """
    try:
        raise FileNotFoundError('flasher_config.yaml file not found')
    except Exception as error:
        print('The flasher_config.yaml file not found'
              'Please check the directory path or a file name', repr(error))


def get_config_param():
    """
    The method reads and returns the config param from YML file.
    :return: config data
    """
    try:
        with open(os.getcwd() + "/repo/fashing_test/flasher_config.yaml", 'r') as stream:
            data = yaml.safe_load(stream)
            return data
    except FileNotFoundError:
        config_file_issue()


def get_args_string(params):
    """
    The method build the argument string to pass the parameters to the flasher shell script.
    :param params: parameter data received from YML file.
    :return: argus string
    """
    print(params)
    # format the argus for sending to sh file
    args_string = []
    sh_file_name = params['ShFILE']

    cmd = "./repo/fashing_test/" + sh_file_name + " "

    sh_script_params = params['PARAMS']
    # change here if sh file use options to accept the flashing params
    count_args = ""
    print(sh_script_params)
    for param in sh_script_params:
        option_ = list(param.keys())[0]
        arg = param[option_]
        print(arg)
        arg_value = os.environ[arg]
        arg = option_ + " " + arg_value
        args_string.append(arg)
        count_args += "'%s' "

    cmd = cmd + count_args % tuple(args_string)
    print("Python cmd: " + cmd)
    return cmd


def start_flashing(cmd):
    """
    The method pass the args to flasher script by calling flash device
    :param cmd: The Shell command to run shell file
    :return: none
    """
    print("-- python_script's start_flashing function --")
    os.system(cmd)


def main():
    """
    1. Call the get_config_param()
    2. Pass the received data to get_args_string() and get the args string.
    3. Call start flashing and start flashing by sending args string.
    """
    cmd = get_args_string(get_config_param())
    start_flashing(cmd)


if __name__ == '__main__':
    sys.exit(main())
