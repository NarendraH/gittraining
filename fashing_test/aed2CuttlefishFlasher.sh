#!/bin/bash -e

##############################################################
# This script is used to flash android variants              #
# automatically inside Asterix2 CI/CT chain                  #
#                                                            #
# For information, how to use this script, run               #
# aed2CuttlefishFlasher.sh -h                                #
#                                                            #
#                                                            #
# Authors:                                                   #
#     Mahfuzur Rahman <mdmahfuzur.rahman@elektrobit.com>     #
#                                                            #
##############################################################

# Init method, called before flashing is started
# This method makes sure:
#     1. devenv has been sourced properly
#     2. Workspace is set, if not triggered from jenkins

function init_environment {
   echo "init_environment"
   echo "CUTTLEFISH_IMAGE_PATH: $CUTTLEFISH_IMAGE_PATH";
   echo "CUTTLEFISH_WORKSPACE: $CUTTLEFISH_WORKSPACE";
   echo "INSTANCE_COUNT: $INSTANCE_COUNT";

}

# Start cuttlefish emulator
#   1. This method is the entry point to the script to start emulator
#   2. This method kills all instances of the emualtors and
#   3. Then starts new instance at a given workspace.

function start_cuttlefish_instances() {
    echo "start_cuttlefish_instances"
    echo "CUTTLEFISH_IMAGE_PATH: $CUTTLEFISH_IMAGE_PATH";
    echo "CUTTLEFISH_WORKSPACE: $CUTTLEFISH_WORKSPACE";
    echo "INSTANCE_COUNT: $INSTANCE_COUNT";
}


while getopts f:w:i: flag
do
    case "${flag}" in
        f) CUTTLEFISH_IMAGE_PATH=${OPTARG};;
        w) CUTTLEFISH_WORKSPACE=${OPTARG};;
        i) INSTANCE_COUNT=${OPTARG};;

    esac
done



## Init the environment ##

init_environment


start_cuttlefish_instances



