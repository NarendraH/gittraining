import os
import yaml


def store_config_param():
    with open(os.getcwd() + '/repo/fashing_test/flasher_config.yaml', 'w+') as flash_file:
        print("Image PATH ", os.environ['CUTTLEFISH_IMAGE_PATH'])
        params = {
            "PRODUCT" : os.environ['PRODUCT'],
            "PROJECT" : os.environ['PROJECT']
        }
        yaml.dump(params, flash_file, default_flow_style=False)
        flash_file.close()


def main():
    store_config_param()


if __name__ == '__main__':
    main()

