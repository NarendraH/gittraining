#!/usr/bin/env python3
import os
import sys
sys.path.append(os.environ['WORKSPACE'])
from flasher import Flasher
import yaml
import subprocess


class Asterix_Flasher(Flasher):

    def __init__(self):
        print("Asterix_Flasher")

    def get_environment(self, product):
        print("get_environment")
        conf = None
        with open(r'config.yaml') as file:
            conf = yaml.load(file, Loader=yaml.FullLoader)

        for prod in conf:
            try:
                return prod[product]
            except KeyError:
                continue

    def flash_device(self):
        print("flash_device")

    def reboot_device(self):
        print('reboot_device')

    def wait_for_device(self):
        print('wait_for_device')


def main():
    print('############# Execution started ############')
    # os.system('./generic_flasher.sh ')
    flasher = Asterix_Flasher()
    #out = input("Please add the product name: \n")
    environment_param = flasher.get_environment("Asterix")
    print(environment_param)

    # called flash devices to shell script
    subprocess.check_output(['bash', '-c', 'source generic_flasher.sh; '
                                           'flash_device {} {} {} {}'.format(str(environment_param['name']),
                                                                             str(environment_param['serialnumber']),
                                                                             str(environment_param['port']),
                                                                             str(environment_param['productdir']))])


if __name__ == '__main__':
    main()
